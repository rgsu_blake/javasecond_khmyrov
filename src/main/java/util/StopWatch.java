package util;

public class StopWatch {
    private static long startTime;

    public static long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public static long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }
}
