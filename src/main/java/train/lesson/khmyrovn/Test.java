package train.lesson.khmyrovn;

import java.util.*;

public class Test {
    static int[] sortInsertings(int[] number) {
        int key;
        for (int i = 1; i < number.length; i++) {
            key = number[i];
            int j = i - 1;
            while (j >= 0 && number[j] > key) { //if previous el-t less then KEY
                number[j + 1] = number[j];
                j--;
            }
            number[j + 1] = key;
        }
        return number;
    }

    static int[] sortSelecting(int[] number) {
        for (int i = 0; i < number.length; i++) {
            int min = number[i];
            int minIndex = i;
            for (int j = i + 1; j < number.length; j++) {
                if (number[j] < min) {
                    min = number[j];
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                int tmp = number[i];
                number[i] = number[minIndex];
                number[minIndex] = tmp;
            }
        }
        return number;
    }

    static int[] sortBubbles(int[] number) {
        for (int i = 0; i < number.length - 1; i++) {
            for (int j = 0; j < number.length - 1 - i; j++) {
                if (number[j] > number[j + 1]) {
                    int tmp = number[j];
                    number[j] = number[j + 1];
                    number[j + 1] = tmp;
                }
            }
        }
        return number;
    }

    static int[] sort(int[] left, int[] right) {
        int size = left.length + right.length;
        int[] newMas = new int[size];
        int j = 0;
        int k = 0;
        for (int i = 0; i < size; i++) {
            if (j == left.length) {
                newMas[i] = right[k++];
            } else if (k == right.length) {
                newMas[i] = left[j++];
            } else {
                if (left[j] < right[k]) {
                    newMas[i] = left[j++];
                } else {
                    newMas[i] = right[k++];
                }
            }
        }
        return newMas;
    }

    static int[] sortMerging(final int[] number) {
        if (number.length < 2) return number;
        int middle = number.length / 2;
        int[] left = Arrays.copyOfRange(number, 0, middle);
        int[] right = Arrays.copyOfRange(number, middle, number.length);
        return sort(sortMerging(left), sortMerging(right));
    }

    static boolean containsDuplicates(int[] numbers) {
        final int MAXSIZE = 99999;
        boolean[] bitMap = new boolean[MAXSIZE + 1];  //all boolean variables == false
        for (int item : numbers) {
            if (!(bitMap[item] ^= true)) {
                return true;
            }
        }
        return false;
    }

    static boolean containsDuplicates1(int[] numbers) {
        Set<Integer> hashSet = new HashSet<>();
        for (int numb : numbers) {
            hashSet.add(numb);
        }
        return hashSet.size() != numbers.length;
    }

    static boolean containsDuplicates2(int[] numbers) {
        boolean isTrue = false;
        for (int i = 0; i < numbers.length; i++) {
            if (isTrue) {
                break;  //проверка чтоб не проверять остальные элементы если уже найден дупликат
            }
            int curDig = numbers[i];
            for (int j = i + 1; j < numbers.length; j++) { //проверяем правую часть на совпадение
                if (numbers[j] == curDig) {
                    isTrue = !isTrue;//false
                }
            }
        }
        return isTrue;
    }
}
