package net.rgsu.training.java.second.khmyrovn;

import java.util.*;

import static util.StopWatch.*;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
        ArrayList arrayList = new ArrayList(10_000_000);
        DynamicArray dynamicArray = new DynamicArray(10_000_000);
    }

    static long dynamicAddToEnd(DynamicArray dynamicArray) {
        start();
        for (int i = 0; i < 10_000_000; i++) {
            dynamicArray.add(i, new Object());
        }
        return getElapsedTime();
    }

    static long listAddToEnd(ArrayList list) {
        start();
        for (int i = 0; i < 10_000_000; i++) {
            list.add(i, new Object());
        }
        return getElapsedTime();
    }

    static long dynamicAddToMiddle(DynamicArray dynamicArray) {
        start();
        for (int i = 0; i < 200_000; i++) {
            dynamicArray.add(i / 2, new Object());
        }
        return getElapsedTime();
    }

    static long listAddToMiddle(ArrayList list) {
        start();
        for (int i = 0; i < 200_000; i++) {
            list.add(i / 2, new Object());
        }
        return getElapsedTime();
    }

    static long dynamicAddToBeginning(DynamicArray dynamicArray) {
        start();
        for (int i = 0; i < 200_000; i++) {
            dynamicArray.add(0, new Object());
        }
        return getElapsedTime();
    }

    static long listAddToBeginning(ArrayList list) {
        start();
        for (int i = 0; i < 200_000; i++) {
            list.add(0, new Object());
        }
        return getElapsedTime();
    }

    static long removeDynamicArrayEnd(DynamicArray array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(i);
        }
        return getElapsedTime();
    }

    static long removeArrayListEnd(ArrayList array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(i);
        }
        return getElapsedTime();
    }

    static long removeDynamicArrayMiddle(DynamicArray array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(i / 2);
        }
        return getElapsedTime();
    }

    static long removeArrayListMiddle(ArrayList array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(i / 2);
        }
        return getElapsedTime();
    }

    static long removeDynamicArrayBeginning(DynamicArray array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(0);
        }
        return getElapsedTime();
    }

    static long removeArrayListBeginning(ArrayList array) {
        start();
        int size = array.size();
        for (int i = size - 1; i >= 0; i--) {
            array.remove(0);
        }
        return getElapsedTime();
    }
}

