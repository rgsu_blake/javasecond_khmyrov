package net.rgsu.training.java.second.khmyrovn;

public class DynamicArrayTest {
    static void testAdding(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " expected!");
        } else {
            System.out.println(testName + " failed! Expected " + true + ", actual " + actual);
            System.out.println("Array is full! The size was increased to 1.5time!");
            System.out.println();
        }
    }

    static void testRemoving(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " expected!");
        } else {
            System.out.println(testName + " failed! Expected " + true + ", actual " + actual);
            System.out.println("Element not found!");
            System.out.println();
        }
    }

    static void testContaining(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " expected!");
        } else {
            System.out.println(testName + " failed! Expected " + true + ", actual " + actual);
            System.out.println("Element not containing in array!");
            System.out.println();
        }
    }

    static void testVoidAdding(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " expected!");
        } else {
            System.out.println(testName + " failed! Expected " + true + ", actual " + actual);
            System.out.println("Index not found!");
            System.out.println();
        }
    }

    static void testObjSetting(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " expected!");
        } else {
            System.out.println(testName + " failed! Expected " + true + ", actual " + actual);
            System.out.println("Incorrect index!");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        DynamicArray<Object> array0 = new DynamicArray<>();
        System.out.println("DynamicArrayTest.testSize = " + array0.size());
        DynamicArray<Object> array = new DynamicArray<>(5);
        System.out.println("DynamicArrayTest.testSize = " + array.size());
        boolean isBoolean = array.add("Nikita");
        testAdding("DynamicArrayTest.add(\"Nikita\")", true, isBoolean);
        isBoolean = array.add("Roma");
        testAdding("DynamicArrayTest.add(\"Roma\")", true, isBoolean);
        isBoolean = array.add(777);
        testAdding("DynamicArrayTest.add(777)", true, isBoolean);
        Object obj777 = array.get(6);//==777(Convert to Object if must remove how type Object)
        isBoolean = array.remove(obj777);
        testRemoving("DynamicArrayTest.remove(777)", true, isBoolean);
        isBoolean = array.remove(obj777);
        testRemoving("DynamicArrayTest.remove(777)", true, isBoolean);
        isBoolean = array.contains("Nikita");
        testContaining("DynamicArrayTest.contains(\"Nikita\")", true, isBoolean);
        array.add(4, "Petya");
        if (array.get(4).equals("Petya")) {
            isBoolean = true;
        } else {
            isBoolean = false;
        }
        testVoidAdding("DynamicArrayTest.add(3,\"Petya\")", true, isBoolean);
        Object newObj = array.set(0, 888);//previous
        Object obj888 = array.get(0);
        if (array.get(0).equals(obj888)) {
            isBoolean = true;
        } else {
            isBoolean = false;
        }
        testObjSetting("DynamicArrayTest.set(0,888)", true, isBoolean);
        System.out.println("DynamicArrayTest.get(4)");
        newObj = array.get(4);
        System.out.println(newObj);
        System.out.println("DynamicArrayTest.remove(5)");
        newObj = array.remove(5);
        System.out.println(newObj);
        System.out.println("DynamicArrayTest.indexOf(\"Petya\")");
        System.out.println(array.indexOf("Petya"));
        Object[] mas = array.toArray();
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i]+" ");
        }
    }
}
