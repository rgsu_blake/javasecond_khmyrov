package net.rgsu.training.java.second.khmyrovn;

public class DynamicArray<T> {
    private Object[] arrayMas;

    DynamicArray() {
        this.arrayMas = new Object[10];
    }

    DynamicArray(int initCapacity) throws NegativeArraySizeException {

        if (initCapacity > 0) {
            arrayMas = new Object[initCapacity];
        } else if (initCapacity == 0) {
            arrayMas = new Object[0];
        } else {
            throw new NegativeArraySizeException("Negativ size of array -> " + initCapacity);
        }


    }

    int size() {
        return arrayMas.length;
    }

    boolean add(Object e) {
        boolean isAdded;
        if (size() == 0) {
            isAdded = false;
        } else if (size() == 1 & arrayMas[0] == null) {
            arrayMas[0] = e;
            isAdded = true;
        } else if (arrayMas[size() - 1] != null) {
            Object[] newMas = arrayMas.clone();
            arrayMas = new Object[newMas.length * 3 / 2];
            for (int i = 0; i < newMas.length; i++) {
                arrayMas[i] = newMas[i];
            }
            isAdded = false;
        } else {
            arrayMas[size() - 1] = e;
            isAdded = true;
        }
        return isAdded;
    }

    void add(int i, Object e) {
        if (i < 0 && i >= size()) {
            throw new ArrayIndexOutOfBoundsException("Incorrect index -> " + i);
        } else {
            Object[] newMas = new Object[size() + 1];
            for (int j = 0; j < i; j++) {
                newMas[j] = arrayMas[j];
            }
            newMas[i] = e;
            for (int j = i + 1; j < newMas.length; j++) {
                newMas[j] = arrayMas[j - 1];
            }
            arrayMas = new Object[newMas.length];
            arrayMas = newMas;
        }
    }

    Object remove(int i) {
        try {
            return arrayMas[i] = null;
        } catch (ArrayIndexOutOfBoundsException exe) {
            System.out.println(exe + " Out of range the array!");
            return null;
        }

    }

    boolean remove(Object e) {
        boolean isRemoved = false;
        if (e == null) {
            for (int i = 0; i < size(); i++) {
                if (arrayMas[i] == null) {
                    isRemoved = true;
                    break;
                }
            }
        } else {
            for (int i = 0; i < size(); i++) {
                if (e.equals(arrayMas[i])) {
                    arrayMas[i] = null;
                    isRemoved = true;
                    break;
                }
            }
        }
        return isRemoved;
    }

    Object get(int i) {
        try {
            return arrayMas[i];
        } catch (ArrayIndexOutOfBoundsException exe) {
            System.out.println(exe + " Out of range the array!");
            return null;
        }
    }

    Object set(int i, Object e) {
        try {
            if (i >= 0) {
                arrayMas[i] = e;
            }
        } catch (ArrayIndexOutOfBoundsException exe) {
            System.out.println(exe + " Out of range the array!");
        }
        if (i == 0) {
            return arrayMas[size() - 1];
        } else {
            return arrayMas[i - 1];
        }

    }

    int indexOf(Object e) {
        if (e == null) {
            for (int i = 0; i < size(); i++) {
                if (arrayMas[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size(); i++) {
                if (e.equals(arrayMas[i])) {
                    return i;
                }
            }
        }
        System.out.println("This object was not found");
        return -1;
    }

    boolean contains(Object e) {
        boolean isContains = false;
        if (e == null) {
            for (int i = 0; i < size(); i++) {
                if (arrayMas[i] == null) {
                    isContains = true;
                    break;
                }
            }
        } else {
            for (int i = 0; i < size(); i++) {
                if (e.equals(arrayMas[i])) {
                    isContains = true;
                    break;
                }
            }
        }
        return isContains;
    }

    Object[] toArray() {
        return arrayMas.clone();
    }
}
